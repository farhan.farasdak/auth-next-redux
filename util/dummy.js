export const sum = (a, b) => a + b;

export const multiply = (a, b) => {
  if (a < 0 || b < 0) {
    throw new Error('Value must be greater than 0');
  }
  return a * b;
};

export const cloneArray = (array) => [...array];

export const pokemonAPI = async () => {
  const resp = await fetch('https://pokeapi.co/api/v2/pokemon/ditto');
  const data = await resp.json();
  return data;
};
