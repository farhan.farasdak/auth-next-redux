import { insertUserBiodata } from './user';

jest.mock('firebase/database', () => ({
  set: () => {},
  ref: () => {},
  getDatabase: () => {},
}));

test('insertUserBiodata: successfully insert biodata', async () => {
  const id = 'sembarangid';
  const userData = { data: 'sembarangdata' };

  const data = await insertUserBiodata(id, userData);
  expect(data).toEqual('ok');
});
