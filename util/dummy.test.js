import {
  cloneArray, multiply, pokemonAPI, sum,
} from './dummy';

test('sum: adding number successfully', () => {
  expect(sum(1, 2)).toBe(3);
});

test('sum: cannot sum string', () => {
  expect(sum('1', '2')).not.toBe(3);
});

test('multiply: multiply number successfully', () => {
  expect(multiply(2, 4)).toBe(8);
});

test('multiply: error raised if value is greated than 0', () => {
  expect(() => { multiply(-2, 5); }).toThrowError('Value must be greater than 0');
  expect(() => { multiply(2, -5); }).toThrowError('Value must be greater than 0');
  expect(() => { multiply(-2, -5); }).toThrowError('Value must be greater than 0');
});

test('cloneArray: properly clones array', () => {
  const array = [1, 2, 3];
  const clonedArray = cloneArray(array);
  expect(clonedArray).toEqual(array);
});

global.fetch = jest.fn().mockImplementation(() => Promise.resolve({
  json: () => ({ weight: 40 }),
}));

test('pokemonAPI: retrieve pokemon API successfully', async () => {
  const data = await pokemonAPI();
  expect(
    data,
  ).toEqual(expect.objectContaining({ weight: 40 }));
});
