import dynamic from 'next/dynamic';

const DynamicDocument = dynamic(() => import('../pdf'), {
  ssr: false,
});

export default DynamicDocument;
