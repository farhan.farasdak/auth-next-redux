import {
  Document, Page, PDFViewer, Text, View, StyleSheet, Image,
} from '@react-pdf/renderer';

const styles = StyleSheet.create({
  body: {
    padding: 20,
  },
});

function PDF({ userData }) {
  return (
    <PDFViewer width="75%" height="750px">
      <Document title="CV">
        <Page style={styles.body}>
          <View>
            <Image src={userData.profileUrl} style={{ width: '100px' }} />
          </View>
          <View>
            <Text style={{ marginTop: '10px' }}>
              Email :
              {userData.email}
            </Text>
            <Text>
              Job :
              {' '}
              {userData.job}
            </Text>
            <Text>
              Age :
              {' '}
              {userData.age}
            </Text>
          </View>
        </Page>
      </Document>
    </PDFViewer>
  );
}

export default PDF;
