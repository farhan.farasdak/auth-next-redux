import { Component } from 'react';
import { ControlBar, Player } from 'video-react';
import Button from '../button';

class VideoPlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      source: props.src,
    };

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.changeVolume = this.changeVolume.bind(this);
  }

  play() {
    this.player.play();
  }

  pause() {
    alert('Pausing videos!');
    this.player.pause();
  }

  changeVolume(steps) {
    return () => {
      const { player } = this.player.getState();
      this.player.volume = player.volume + steps;
    };
  }

  render() {
    const { source } = this.state;

    return (
      <div>
        <Player ref={(player) => {
          console.log(player);
          this.player = player;
        }}
        >
          <source src={source} />
          <ControlBar autoHide />
        </Player>

        <Button title="Play" onClick={this.play} />
        <Button title="Pause" onClick={this.pause} />
        <Button title="Gedein Suara" onClick={this.changeVolume(0.1)} />
        <Button title="Kecilin Suara" onClick={this.changeVolume(-0.1)} />
      </div>
    );
  }
}

export default VideoPlayer;
